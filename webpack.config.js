
var debug = process.env.NODE_ENV !== "production";
var appFolder = 'src-js';
var webpack = require('webpack');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var WebpackBundleSizeAnalyzerPlugin = require('webpack-bundle-size-analyzer').WebpackBundleSizeAnalyzerPlugin;

var analyzerConfig = {
  analyzerMode: 'server',
  analyzerHost: '127.0.0.1',
  analyzerPort: 8888,
  reportFilename: 'report.html',
  openAnalyzer: true,
  generateStatsFile: false,
  statsFilename: 'stats.json',
  statsOptions: null,
  logLevel: 'info'
};

var devPlugins = [
  // new BundleAnalyzerPlugin(analyzerConfig),
  new WebpackBundleSizeAnalyzerPlugin('./reports/plain-report.txt'),
]
var prodPlugins = [
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.optimize.UglifyJsPlugin({ mangle: false, comments: false, sourcemap: false }),
  new webpack.optimize.AggressiveMergingPlugin(),
]

module.exports = {
  context: __dirname,
  devtool: debug ? "source-map" : false,
  entry: `./${appFolder}/main.js`,
  output: {
    path: __dirname + "/public/js",
    filename: "w.bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['es2015'],
            babelrc: false,
         }
        }],
      },
    ],
  },
  plugins: debug ? [...devPlugins,...prodPlugins] : prodPlugins,
};
