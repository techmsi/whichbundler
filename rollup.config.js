// rollup.config.js
import babel from 'rollup-plugin-babel';
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import uglify from 'rollup-plugin-uglify';

const appFolder = 'src-js';
const browserOnly = true;

export default {
  entry: `${__dirname}/${appFolder}/main.js`,
  format: browserOnly ? 'iife' : 'umd',
  dest: 'public/js/r.bundle.js',
  plugins: [
    resolve({
        jsnext  : true,
        main    : true,
        browser : true
    }),
    commonjs({
    namedExports : {
        "node_modules/ramda/index.js" : Object.keys(require("ramda"))
      }
    }),
    babel({
      babelrc: false,
      presets: ["es2015-rollup"],
      exclude: 'node_modules/**',
    }),
    uglify()
   ]
}
