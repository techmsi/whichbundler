const _log = console.log.bind(console);
_log('DEBUG')
export const print = o => JSON.stringify(o, null, 2);
export const debug = o => _log(print(o));
