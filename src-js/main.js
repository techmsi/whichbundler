import chain from 'ramda/src/chain';
import { debug, print } from './utils/debug';

var square = x => x * x;

var squares = chain(square, [10, 2, 3, 4, 5]);
debug(squares);

document.getElementById('response').innerHTML = squares;
